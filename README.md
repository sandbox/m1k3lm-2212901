commerce-paytpv
==========================

##Módulo de pago por tarjeta de crédito para Drupal Commerce a través de la pasarela de PayTpv

Instalación
----------------
Es posible hacer la instalación vía administración de Drupal si tenemos activado el módulo update manager:

1.- Estando registrado con permisos de administrador vamos a Site settings -> modules -> install new module. En caso de no tener activado el update manager, podemos, descomprimir el fichero commerce_ceca.zip, subir por FTP la carpeta commerce_ceca al directerio site/all/modules y seguir en el punto 4

2.- Elegimos el fichero commerce_cea.zip que nos hemos descargado del pedido.

3.- Introducimos los datos de acceso FTP
Una vez instalado procedemos a activarlo para eso en modules -> Commerce payment, marcamos la casilla "enabled" del módulo y guardamos la configuración.

Configuración del método de pago
--------------------------------

1.- Accedemos a store settings -> payment methods
Debería aparecernos el método de pago como inhabilidato, lo habilitmaos.

2.- Una vez habilitado pulsamos en editar, en la siguiente pantalla editamos la acción, donde tendremos que rellenar los campos User code, Client code, terminal y password... con los datos que tenemos de [PAYTPV]( https://PAYTPV.com).

Configuración producto PAYTPV
-----------------------------

1.- En [PAYTPV]( https://PAYTPV.com) -> clientes -> Mis productos -> Configurar -> productos

2.- Editamos el producto correspondiente y en Url Notififivación http ponemos http://{dirección drupal}?q=commerce_paytpv/notify   sustituyendo {dirección drupal} por el dominio y ubicación de nuestra isntalación Drupal Commerce